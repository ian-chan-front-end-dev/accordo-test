import fs from 'fs';
import {map, split, forEach, upperCase} from 'lodash';

//convert input commands from file input
const commands = fs.readFileSync('commands.txt', 'utf8')
	.trim()
	.split('\n')
	.map(row => row.split('\t'));

//The bus object
const Bus = {
	//initial value
	x: 0,
	y: 0,
	f: 'SOUTHWEST',
	//constraint
	carparkX: 5,
	carparkY: 5,
	//commands
	reset() {
		this.x = 0;
		this.y = 0;
		this.f = 'SOUTHWEST';
		console.log('Reset bus simulator')
	},
	// placing bus to location of x, y and facing direction
	place(x, y, f) {
		//ignore all commands before PLACE, resetting
		this.reset();
		if ((x < this.carparkX || this.x + x < this.carparkX) 
			&& (y < this.carparkY || this.y + y < this.carparkY)) {
			this.x = x;
			this.y = y;
			this.f = f;
			console.log(`PLACE ${this.x},${this.y},${this.f}`);
		} else {
			this.busExitAlert('place');
		}
	},
	// moving one box to the current facing direction
	move() {
		console.log('MOVE');
		if(!this.go('move'))
			this.busExitAlert('move');
	},
	// turning direction to the left of current facing
	left() {
		console.log('LEFT');
		if(!this.go('left'))
			this.noDirection();
	},
	//turning direction to the right of current facing
	right() {
		console.log('RIGHT');
		if(!this.go('right'))
			this.noDirection();
	},
	// output bus current location
	report() {
		console.log(`REPORT\n\nOutput: ${this.x},${this.y},${this.f}\n\n`);
	},
	// execute bus command
	go(command = null, facing = this.f) {

		switch(facing) {
			case 'NORTH':
				if (command === 'move') {
					if(this.y + 1 < this.carparkY) this.y += 1;
					else return false;
				} else {
					(command === 'left')
						? this.f = 'WEST'
						: this.f = 'EAST';
				}
				break;

			case 'SOUTH':
				if (command === 'move') {
					if(this.y - 1 >= 0) this.y -= 1;
					else return false;
				} else {
					(command === 'left')
						? this.f = 'EAST'
						: this.f = 'WEST';
				}
				break;

			case 'EAST':
				if (command === 'move') {
					if(this.x + 1 < this.carparkX) this.x += 1; 
					else return false;
				} else {
					(command === 'left')
						? this.f = 'NORTH'
						: this.f = 'SOUTH';
				}
				break;

			case 'WEST':
				if (command === 'move') {
					if(this.x - 1 >= 0) this.x -= 1; 
					else return false;
				} else {
					(command === 'left')
						? this.f = 'SOUTH'
						: this.f = 'NORTH';
				}
				break;

			default: 
				return false
				break;
		}
		return true;
	},
	// alerts
	busExitAlert(command) {
		console.log(`Bus will exit the carpark, ${upperCase(command)} command ignore!`);
	},
	noDirection() {
		console.log('Invalid facing direction provided, command ignore!');
	}
};

//switching between bus commands
const executeCommand = function(value) {
	//split command and params if any
	const arr = value.trim().split(' ', 2);
	//command
	switch(arr[0]) {
		case 'PLACE':
			const params = arr[1].split(',');
			Bus.place(parseInt(params[0]), parseInt(params[1]), params[2]);
			break;

		case 'MOVE': 
			Bus.move();
			break;

		case 'LEFT':
			Bus.left();
			break;

		case 'RIGHT':
			Bus.right();
			break;

		case 'REPORT': 
			Bus.report();
			break;

		default:
			//do nothing, this can prevent \t at end of line in text file
			break;
	} 
};

//iterate and execute each command in the sequence
commands.forEach(
	row => row.map(
		value => {
			executeCommand(value);
		}
	)
);