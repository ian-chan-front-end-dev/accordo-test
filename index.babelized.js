'use strict';

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _lodash = require('lodash');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//convert input commands from file input
var commands = _fs2.default.readFileSync('commands.txt', 'utf8').trim().split('\n').map(function (row) {
	return row.split('\t');
});

//The bus object
var Bus = {
	//initial value
	x: 0,
	y: 0,
	f: 'SOUTHWEST',
	//constraint
	carparkX: 5,
	carparkY: 5,
	//commands
	reset: function reset() {
		this.x = 0;
		this.y = 0;
		this.f = 'SOUTHWEST';
		console.log('Reset bus simulator');
	},

	// placing bus to location of x, y and facing direction
	place: function place(x, y, f) {
		//ignore all commands before PLACE, resetting
		this.reset();
		if ((x < this.carparkX || this.x + x < this.carparkX) && (y < this.carparkY || this.y + y < this.carparkY)) {
			this.x = x;
			this.y = y;
			this.f = f;
			console.log('PLACE ' + this.x + ',' + this.y + ',' + this.f);
		} else {
			this.busExitAlert('place');
		}
	},

	// moving one box to the current facing direction
	move: function move() {
		console.log('MOVE');
		if (!this.go('move')) this.busExitAlert('move');
	},

	// turning direction to the left of current facing
	left: function left() {
		console.log('LEFT');
		if (!this.go('left')) this.noDirection();
	},

	//turning direction to the right of current facing
	right: function right() {
		console.log('RIGHT');
		if (!this.go('right')) this.noDirection();
	},

	// output bus current location
	report: function report() {
		console.log('REPORT\n\nOutput: ' + this.x + ',' + this.y + ',' + this.f + '\n\n');
	},

	// execute bus command
	go: function go() {
		var command = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
		var facing = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.f;


		switch (facing) {
			case 'NORTH':
				if (command === 'move') {
					if (this.y + 1 < this.carparkY) this.y += 1;else return false;
				} else {
					command === 'left' ? this.f = 'WEST' : this.f = 'EAST';
				}
				break;

			case 'SOUTH':
				if (command === 'move') {
					if (this.y - 1 >= 0) this.y -= 1;else return false;
				} else {
					command === 'left' ? this.f = 'EAST' : this.f = 'WEST';
				}
				break;

			case 'EAST':
				if (command === 'move') {
					if (this.x + 1 < this.carparkX) this.x += 1;else return false;
				} else {
					command === 'left' ? this.f = 'NORTH' : this.f = 'SOUTH';
				}
				break;

			case 'WEST':
				if (command === 'move') {
					if (this.x - 1 >= 0) this.x -= 1;else return false;
				} else {
					command === 'left' ? this.f = 'SOUTH' : this.f = 'NORTH';
				}
				break;

			default:
				return false;
				break;
		}
		return true;
	},

	// alerts
	busExitAlert: function busExitAlert(command) {
		console.log('Bus will exit the carpark, ' + (0, _lodash.upperCase)(command) + ' command ignore!');
	},
	noDirection: function noDirection() {
		console.log('Invalid facing direction provided, command ignore!');
	}
};

//switching between bus commands
var executeCommand = function executeCommand(value) {
	//split command and params if any
	var arr = value.trim().split(' ', 2);
	//command
	switch (arr[0]) {
		case 'PLACE':
			var params = arr[1].split(',');
			Bus.place(parseInt(params[0]), parseInt(params[1]), params[2]);
			break;

		case 'MOVE':
			Bus.move();
			break;

		case 'LEFT':
			Bus.left();
			break;

		case 'RIGHT':
			Bus.right();
			break;

		case 'REPORT':
			Bus.report();
			break;

		default:
			//do nothing, this can prevent \t at end of line in text file
			break;
	}
};

//iterate and execute each command in the sequence
commands.forEach(function (row) {
	return row.map(function (value) {
		executeCommand(value);
	});
});
